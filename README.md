# Cirquits

This repository contains the hardware requirements for the Project. Additionally
you will find cirquits and PCB plan which are also used in this project.

The whole project is designed for five plant pots, which will have a separate
sensors, separate hydration indicators and can be hydrated also separate.

Required main components:
*  [5x]  **capacitiv hydration sensor** to determine the hydration level
*  [5x]  **mini water pump** for automatic hydration of the plants
*  [35x] **blue leds** as hydration level indicator
*  [5x]  **red leds** as error indicator
*  [5x]  **SIPO shift register** to pass data to the leds
*  [1x]  **NodeMCU** to provide hydration data via network and as remote hydration trigger
*  [1x]  **Respberry Pi 3** to store hydration data, drigger hydration and provide some other functions